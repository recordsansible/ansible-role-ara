This project has been retired
-----------------------------

The Ansible role has been merged into an Ansible collection.

The code is available on GitHub: https://github.com/ansible-community/ara-collection

